import xlwings as xw
import pandas as pd
import json

data = json.load(open('data.txt'))

# Create a Pandas dataframe from the data.
df_timeline = pd.DataFrame({' ':[  data["years"],
                           data["installationBeginDate"],
                           data["installationEndDate"],                         
                           data["operationStartDate"],
                           data["operationEndDate"]]}
                        )

df_revenues = pd.DataFrame({' ':[data["sites"][0]["ecms"][0]["consumptionSavings"],
                        data["sites"][0]["ecms"][0]["demandSavings"],
                        data["sites"][0]["ecms"][0]["sellingSavings"]]}
                        )

df_disbursement = pd.DataFrame({' ':[(100-data["contractorShareOfEnergySavings"])/100]}
                        )

df_cost_structure = pd.DataFrame({' ':[data["totalAuditCosts"],
                        data["totalEquipmentCost"],
                        data["totalInstallationCost"],
                        data["markupCost"]]}
                        )

df_op_expenses = pd.DataFrame({' ':['Admin',
                        'Insur',
                        data["oMAnnualFees"],'M&V','Rent','PM']}
                        )

#load workbook which is the template
wb = xw.Book('Fixed_Hybrid.xlsx')

ws = wb.sheets['Assumptions']

ws.range('C4').options(index=False).value = df_timeline
ws.range('C11').options(index=False).value = df_revenues
ws.range('C19').options(index=False).value = df_disbursement
ws.range('C23').options(index=False).value = df_cost_structure
ws.range('C34').options(index=False).value = df_op_expenses

wb = xw.Book('Fixed_Hybrid.xlsx')

wb.save()

xw.apps[0].quit()